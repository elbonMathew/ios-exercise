# iOS Exercise

This repository is used for an iOS exercise as part of an interview.

The project does the following

- Fetch a JSON from the following link https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json
- Parse the JSON and display it in a `UICollectionView`
- Download the images using the image ref in the JSON and display it as part of the collectionView.
- Show a detail view with the description on tapping on each of the `UICollectionView` cells

### Project Attributes

- [x] Universal application
- [x] Include both portrait and landscape orientation
- [x] Include phones from iOS 8
- [x] Include auto layout

**Project Architecture**

This Project uses an MVC pattern as the project is not too complicated and can be built using a single viewController. It uses a model, that is a `Decodable` class, which uses `JSONDecoder` to decode the JSON into the appropriate objects.
